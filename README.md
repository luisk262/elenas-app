# README #

Se requiere hacer una app para registrar tareas como se muestra a continuación:
Escribe un REST API en Django para suplir los siguientes requerimientos Python - Django.
	* Los usuarios se deben autenticar
	* Las tareas son privadas. Solo las puede administrar su dueño
	* Los usuarios pueden agregar, editar, eliminar y marcar como completa/incompleta las tareas
	* El listado de tareas debe ser paginado
	* Agregar validaciones, como no aceptar tareas sin descripción, etc
	* Buscar por descripción
	* Escribir test unitarios en el primer commit
### Requerimientos ###
Requeriminetos para ejecutar el proyecto
	* Python 3.10.0
	* Django version 3.2.9
	* Mysql Ver 8.0.26

### Documentación ###
Puede encontrar la documentacion en las siguientes rutas
	* http://localhost:8000/
	* http://localhost:8000/swagger/
### Demo ###
* https://elenas-app.herokuapp.com/
user: admin pass: admin

### Crear usuario administrativo ###
Debe crear un usuario para poder ingresar al panel administrativo
	* pip manage.py createsuperuser

### Panel administrativo ###
Ruta para ingresar al panel adminstrativo
	* http://localhost:8000/admin
### Testeo ###
Comandos para testeo
	* pip manage.py test apps.tasks
	* pip manage.py test apps.users
### Contacto ###
Información de contacto
	* Luis Carlos Alvarez 
	* luisk9801@gmail.com


