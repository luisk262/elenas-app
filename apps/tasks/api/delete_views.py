
from django.contrib.auth.models import User
from rest_framework.response import Response
from apps.tasks.api.sequalizer import StatusTaskSerializer, TaskSerializer
from rest_framework import generics, status

from apps.tasks.models import StatusTask, Task


class StatusTaskDestroyAPIView(generics.DestroyAPIView):
    """
    Eliminar estado

    Eliminar logicamente el estado de la tarea
    """
    serializer_class = StatusTaskSerializer

    def delete(self, request, pk=None):
        status_task = self.get_queryset().filter(id=pk).first()
        if status_task:
            status_task.is_deleted = True
            status_task.save()
            return Response({'message': 'Eliminado'}, status=status.HTTP_200_OK)
        return Response({'message': 'No existe una tarea con este identificador'}, status=status.HTTP_404_NOT_FOUND)

    def get_queryset(self):
        return StatusTask.objects.filter(is_deleted=False)


class TaskDestroyAPIView(generics.DestroyAPIView):
    """
    Eliminar tarea

    Eliminar logicamente una tarea
    """

    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(is_deleted=False, user=self.request.user.pk)

    def delete(self, request, pk=None):
        task = self.get_queryset().filter(id=pk, user=request.user.pk).first()
        if task:
            task.is_deleted = True
            task.save()
            return Response({'message': 'Eliminado'}, status=status.HTTP_200_OK)

        return Response({'message': 'No existe una tarea con este identificador'}, status=status.HTTP_404_NOT_FOUND)
