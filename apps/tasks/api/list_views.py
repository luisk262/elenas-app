from apps.tasks.models import StatusTask, Task
from rest_framework import generics
from apps.tasks.api.sequalizer import StatusTaskSerializer, TaskSerializer


class TaskListAPIView(generics.ListAPIView):
    """
    Lista tareas

    Muestra la lista de tareas creadas
    Para filtrar por descripción enviar el parametro "description"
    Ejemplo /tasks/task?description=mi_valor
    """
    
    serializer_class = TaskSerializer

    def get_queryset(self):
        req = self.request
        user_id = req.user.pk
        description = req.query_params.get('description')
        
        if description:
            return Task.objects.filter(is_deleted=False, description__contains=description,user_id=user_id)
        else:
            return Task.objects.filter(is_deleted=False,user_id=user_id)


class StatusTaskListAPIView(generics.ListAPIView):
    """
    Lista estados

    Muestra la lista de estados creadas
    """
    serializer_class = StatusTaskSerializer

    def get_queryset(self):
        return StatusTask.objects.filter(is_deleted=False)
