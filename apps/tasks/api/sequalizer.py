from rest_framework import serializers
from apps.tasks.models import StatusTask, Task


class StatusTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = StatusTask
        exclude = ('is_deleted', 'deleted_date')


class TaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = Task
        exclude = ('is_deleted', 'deleted_date')
        read_only_fields = ['status']

    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description,
            'status_task': instance.status_task.name,
            'user':instance.user.username
        }
