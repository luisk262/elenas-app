
from apps.tasks.api.sequalizer import StatusTaskSerializer, TaskSerializer
from rest_framework import generics

from apps.tasks.models import StatusTask, Task

class StatusTaskDetailAPIView(generics.RetrieveAPIView):
    """
    Detalle estado

    Mostrar detalle de estados de tareas
    """
    
    serializer_class = StatusTaskSerializer

    def get_queryset(self):
        return StatusTask.objects.filter(is_deleted=False)


class TaskDetailAPIView(generics.RetrieveAPIView):
    """
    Detalle Tarea
    
    Mostrar detalle  de tarea
    """
    serializer_class = TaskSerializer

    def get_queryset(self):

        return Task.objects.filter(is_deleted=False,user=self.request.user.pk)
