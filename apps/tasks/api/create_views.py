from django.conf import settings
from rest_framework.response import Response
from apps.tasks.models import StatusTask, Task
from apps.tasks.api.sequalizer import StatusTaskSerializer, TaskSerializer
from rest_framework import generics, status
from apps.users.models import User


class TaskCreateAPIView(generics.CreateAPIView):
    """
    Crear tarea

    Crea una nueva tarea
    """
    serializer_class = TaskSerializer
    queryset = Task.objects.filter(is_deleted=False)

    def post(self, request):
        data = {
            "name": request.data['name'],
            "description": request.data['description'],
            "status_task": request.data['status_task'],
            "user": request.user.pk
        }
        task_serializer = TaskSerializer(data=data)

        if task_serializer.is_valid():
            task_serializer.save()
            return Response(task_serializer.data)
        return Response(task_serializer.errors, status=status.HTTP_201_CREATED)


class StatusTaskCreateAPIView(generics.CreateAPIView):
    """
    Crear estado


    Creacion de un estado para las tareas
    """
    serializer_class = StatusTaskSerializer

    def get_queryset(self):
        return StatusTask.objects.filter(is_deleted=False)
