
from rest_framework.response import Response
from apps.tasks.api.sequalizer import StatusTaskSerializer, TaskSerializer
from rest_framework import generics, status

from apps.tasks.models import StatusTask, Task


class StatusTaskUpdateAPIView(generics.UpdateAPIView):
    """
    Actualizar estado

    Actualiza el nombre del estado
    """
    serializer_class = StatusTaskSerializer

    def get_queryset(self):
        return StatusTask.objects.filter(is_deleted=False)

    def patch(self, request, pk=None):
        status_task = self.get_queryset().filter(id=pk, is_deleted=False).first()
        if status_task:
            status_task_serializer = StatusTaskSerializer(status_task)
            return Response(status_task_serializer.data, status=status.HTTP_200_OK)
        return Response({'error': 'No existe el estado'}, status=status.HTTP_404_NOT_FOUND)


class TaskUpdateAPIView(generics.UpdateAPIView):
    """
    Actualizar Tarea

    Actualiza el nombre, descripción  y estado de la tarea 
    """
    serializer_class = TaskSerializer

    def get_queryset(self):
        return Task.objects.filter(is_deleted=False, user=self.request.user.pk)

    def patch(self, request, pk=None):
        task = self.get_queryset().filter(id=pk, is_deleted=False,
                                          user=self.request.user.pk).first()
        if task:
            task_serializer = TaskSerializer(task)
            return Response(task_serializer.data, status=status.HTTP_200_OK)
        return Response({'error': 'No existe el la tarea'}, status=status.HTTP_404_NOT_FOUND)
