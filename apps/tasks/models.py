from django.conf import settings
from django.db import models
from apps.base.models import BaseModel
from apps.users.models import User

# Create your models here.


class StatusTask(BaseModel):
    name = models.CharField(max_length=50, blank=False, null=False)

    class Meta:
        verbose_name = "Estado de tarea"
        verbose_name_plural = "Estados de tareas"

    def __str__(self) -> str:
        return self.name


class Task(BaseModel):

    name = models.CharField(max_length=50)
    description = models.CharField(
        'Descripcion', max_length=200, blank=False, null=False)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE, blank=True)
    status_task = models.ForeignKey(
        StatusTask, on_delete=models.CASCADE, verbose_name='Estado')

    class Meta:
        verbose_name = "Tarea"
        verbose_name_plural = "Tareas"

    def __str__(self) -> str:
        return self.name
