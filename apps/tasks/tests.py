from django.test import TestCase

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from apps.users.models import User
import json

class TaskTestCase(TestCase):
    access_token = None

    def setUp(self):
        user = User(
            email='luisk__@hotmail.comm',
            name='luis',
            last_name='alvarez',
            username='admin'
        )
        user.set_password('admin')
        user.save()
        client = APIClient()
        response_login = client.post(
            '/api/token/', {
                'username': 'admin',
                'password': 'admin',
            },
            format='json'
        )
        result = json.loads(response_login.content)
        self.access_token = result['access']

    def test_register_task(self):
        """Check register new task"""
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)
        response = client.post(
            '/tasks/task/create/', {
                'name': 'Tarea 1',
                'description': 'Desripcion tarea 1',
                'status_task': 1
            },
            format='multipart'
        )
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_register_status_task(self):
        """Check create status for task"""
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)
        response = client.post(
            '/tasks/status/create/', {
                'name': 'incompleto',
            },
            format='multipart'
        )
        print(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_task(self):
        """Check update task"""
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        create_status = client.post(
            '/tasks/status/create/', {
                'name': 'incompleto',
            },
            format='multipart'
        )
        status = json.loads(create_status.content)

        create_response = client.post(
            '/tasks/task/create/', {
                'name': 'Tarea 1',
                'description': 'test update',
                'status_task': int(status['id'])
            },
            format='multipart'
        )
        task = json.loads(create_response.content)

        response = client.put(
            '/tasks/task/update/'+str(task['id']), {
                "name": "actualizada",
                "description": "actualizada",
                "status_task": int(status['id'])
            },
            format='multipart'
        )
        self.assertEquals(json.loads(response.content), {
                          "id": 2, "name": "actualizada", "description": "actualizada", "status_task": "incompleto", "user": "admin"})

    def test_delete_task(self):
        """Check delete one task user"""
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        client.post(
            '/tasks/status/create/', {
                'name': 'incompleto',
            },
            format='multipart'
        )

        create_response = client.post(
            '/tasks/task/create/', {
                'name': 'Tarea 1',
                'description': 'test_filtro',
                'status_task': 1
            },
            format='multipart'
        )

        response = client.delete(
            '/tasks/task/destroy/1',
            format='multipart'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEquals(json.loads(response.content),
                          {'message': 'Eliminado'})

    def test_filter_task(self):
        """Check filter by description"""

        # Register task for filter test
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.access_token)

        client.post(
            '/tasks/status/create/', {
                'name': 'incompleto',
            },
            format='multipart'
        )

        create_response = client.post(
            '/tasks/task/create/', {
                'name': 'Tarea 1',
                'description': 'test_filtro',
                'status_task': 1
            },
            format='multipart'
        )

        response = client.get(
            '/tasks/task/',
            params={'descripcion': 'test_filtro'},
            format='multipart'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        results = result['results']
        self.assertIsNotNone(results)
