from django.contrib import admin
from apps.tasks.models import StatusTask, Task


class StatusTaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created_date')

class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name','user', 'created_date')

admin.site.register(Task,TaskAdmin)
admin.site.register(StatusTask,StatusTaskAdmin)
# Register your models here.
