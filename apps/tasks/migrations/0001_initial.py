# Generated by Django 3.2.9 on 2021-12-17 16:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='StatusTask',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Eliminado')),
                ('created_date', models.DateField(auto_now_add=True, verbose_name='created_date')),
                ('updated_date', models.DateField(auto_now=True, verbose_name='updated_date')),
                ('deleted_date', models.DateField(auto_now=True, verbose_name='deleted_date,')),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'Estado de tarea',
                'verbose_name_plural': 'Estados de tareas',
            },
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('is_deleted', models.BooleanField(default=False, verbose_name='Eliminado')),
                ('created_date', models.DateField(auto_now_add=True, verbose_name='created_date')),
                ('updated_date', models.DateField(auto_now=True, verbose_name='updated_date')),
                ('deleted_date', models.DateField(auto_now=True, verbose_name='deleted_date,')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=200, verbose_name='Descripcion')),
                ('status_task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tasks.statustask', verbose_name='Estado')),
            ],
            options={
                'verbose_name': 'Tarea',
                'verbose_name_plural': 'Tareas',
            },
        ),
    ]
