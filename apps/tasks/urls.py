from django.urls import path
from apps.tasks.api.create_views import StatusTaskCreateAPIView, TaskCreateAPIView
from apps.tasks.api.delete_views import StatusTaskDestroyAPIView, TaskDestroyAPIView
from apps.tasks.api.detail_views import StatusTaskDetailAPIView, TaskDetailAPIView
from apps.tasks.api.list_views import StatusTaskListAPIView, TaskListAPIView
from apps.tasks.api.update_views import StatusTaskUpdateAPIView, TaskUpdateAPIView

urlpatterns = [
    path('task/', TaskListAPIView.as_view(), name='tasks'),
    path('task/<int:pk>', TaskDetailAPIView.as_view(), name='task_detail'),
    path('task/create/', TaskCreateAPIView.as_view(), name='create_task'),
    path('task/destroy/<int:pk>', TaskDestroyAPIView.as_view(), name='destroy_task'),
    path('task/update/<int:pk>', TaskUpdateAPIView.as_view(), name='update_task'),
    path('status/', StatusTaskListAPIView.as_view(), name='status_tasks'),
    path('status/<int:pk>', StatusTaskDetailAPIView.as_view(), name='status_detail'),
    path('status/create/', StatusTaskCreateAPIView.as_view(),
         name='create_status_task'),
    path('status/destoy/<int:pk>', StatusTaskDestroyAPIView.as_view(),
         name='destroy_status_task'),
    path('status/update/<int:pk>', StatusTaskUpdateAPIView.as_view(),
         name='update_status_task'),
]
