from django.urls import path
from apps.users.api.user_views import user_api_view, user_detail_api_view
from apps.users.api.auth_views import login


urlpatterns = [
    path('user/', user_api_view, name='user'),
    path('user/<int:pk>', user_detail_api_view, name='user_detail'),
]
