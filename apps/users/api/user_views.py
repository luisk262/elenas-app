from rest_framework.response import Response
from rest_framework import status
from apps.users.models import User
from .user_sequializer import UserSequalizer, UserListSequializer
from rest_framework.decorators import api_view

@api_view(['GET', 'POST'])
def user_api_view(request):
    # list
    if request.method == 'GET':
        moneys = User.objects.all()
        serializer_class = UserListSequializer(moneys, many=True)

        return Response(serializer_class.data)

    # update
    elif request.method == 'POST':
        user_sequializer = UserSequalizer(data=request.data)
        if user_sequializer.is_valid():
            user_sequializer.save()
            return Response(user_sequializer.data)
        return Response(user_sequializer.errors, status=status.HTTP_201_CREATED)


@api_view(['GET', 'PUT', 'DELETE'])
def user_detail_api_view(request, pk=None):

    user = User.objects.filter(id=pk).first()
    if user:
        # detail
        if request.method == 'GET':
            user_sequializer = UserSequalizer(user)
            return Response(user_sequializer.data, status=status.HTTP_200_OK)

        # update detail
        elif request.method == 'PUT':
            user_sequializer = UserSequalizer(user, data=request.data)
            if user_sequializer.is_valid():
                user_sequializer.save()
                return Response(user_sequializer.data, status=status.HTTP_200_OK)
            return Response(user_sequializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # delete
        elif request.method == 'DELETE':
            user.delete()
            return Response({'message': 'Eliminado'}, status=status.HTTP_200_OK)

    else:
        return Response({'message': 'Usuario no existe'}, status=status.HTTP_404_NOT_FOUND)
