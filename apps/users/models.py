from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models

# Create your models here.
class User(AbstractBaseUser, PermissionsMixin):
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=True)

    username = models.CharField(max_length=10, unique=True)
    password = models.CharField(max_length=200)
    name = models.CharField('Nombres', max_length=200, blank=True, null=True)

    last_name = models.CharField(
        'Apellidos', max_length=200, blank=True, null=True)
    email = models.CharField(max_length=100, unique=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'name', 'last_name']
    objects = UserManager()

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def __str__(self) -> str:
        return self.username