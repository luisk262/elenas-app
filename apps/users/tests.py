from django.test import TestCase

# Django Rest Framework
from rest_framework.test import APIClient
from rest_framework import status
from .models import User
import json

class UserTestCase(TestCase):
    def setUp(self):
        user = User(
            email='luisk__@hotmail.comm',
            name='luis',
            last_name='alvarez',
            username='admin'
        )
        user.set_password('admin')
        user.save()

    def test_signin_user(self):
        """Check login user"""
        client = APIClient()
        response = client.post(
                '/api/token/', {
                'username': 'admin',
                'password': 'admin'
            },
            format='multipart'
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        result = json.loads(response.content)
        self.assertIn('access', result)